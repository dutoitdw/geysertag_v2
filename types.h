//*****************************************************
// PROJECT	:		GeyserTag
//
//
// UNIT		:		NFC enabling
//
//
// AUTHORS	:		De Wet du Toit
//					Carlo Olivier
//					Henk Kleynhans
//					Bernard Swart
//******************************************************

typedef unsigned char 	bool;
typedef unsigned char 	uint8_t;
typedef signed char 	sint8_t;
typedef unsigned short 	uint16_t;
typedef signed short 	sint16_t;
typedef unsigned long 	uint32_t;
typedef signed long 	sint32_t;

#define TRUE 	1					//werk in hoofletter TRUE en FALSE
#define FALSE	0

#define PORT_INPUT		1			//nie seker waar hierdie gebruik gaan word nie
#define PORT_OUTPUT		0

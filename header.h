//*****************************************************
// PROJECT	:		GeyserTag
//
//
// UNIT		:		NFC enabling
//
//
// AUTHORS	:		De Wet du Toit
//					Carlo Olivier
//					Henk Kleynhans
//					Bernard Swart
//******************************************************

#ifndef __HEADER_H
#define __HEADER_H

//include standard libraries
//#include <htc.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//include special libraries
#include "types.h"
#include "crc16.h"
#include "M24SR.h"
#include "Wire.h"

#endif
